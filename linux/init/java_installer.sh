#!/bin/bash

clear

JDK_8="jdk1.8.0_191"
JDK_10="jdk-10.0.2"
JDK_11="jdk-11.0.1"

EXTENSION="tar.gz"

echo "ESTANGI Java installer"
echo
echo "[1] JDK 1.8.191"
echo "[2] JDK 10.0.2"
echo "[3] JDK 11.0.1"
echo
read -p "Enter version you want to install :" VER

case "$VER" in

1) VERSION=$JDK_8
   ;;
2) VERSION=$JDK_10
   ;;
3) VERSION=$JDK_11
   ;;
*) echo "Wrong choice..."
   exit

esac
echo "Download $VERSION from repository "

groupadd java

JAVA_HOME=/usr/share/java
JAVA_REPO=$JAVA_HOME/jdk-version
JAVA_FOLDER=$JAVA_REPO/$VERSION
JAVA_ARCHIVE=$VERSION.$EXTENSION
JAVA_DOWNLOAD=$JAVA_REPO/$JAVA_ARCHIVE
JAVA_JDK=$JAVA_HOME/jdk

if [ ! -d $JAVA_REPO ]; then
   mkdir $JAVA_REPO
   chgrp java $JAVA_REPO
fi

cd $JAVA_REPO

git archive --remote=ssh://git@gitlab.com/burninghead/java.git HEAD:jdk/linux $JAVA_ARCHIVE | tar -x

if [ ! -f $JAVA_DOWNLOAD ]; then
	echo "Download failed"
	exit
fi

echo "extract Java archive"

tar xzf $JAVA_DOWNLOAD
chgrp -R java $JAVA_FOLDER
chmod -R 770 $JAVA_FOLDER

if [ ! -e $JAVA_JDK ]; then
  echo "Make this version as default one"
  ln -sn $JAVA_FOLDER $JAVA_JDK
  chgrp -R java $JAVA_JDK
fi

echo "Java installation successful"
