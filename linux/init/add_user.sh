#!/bin/bash

read -p "Please enter user name : " USER
read -p "Please enter user password :" PSWD
echo "Create user $USER account:"
useradd -m $USER
echo -e "$PSWD\n$PSWD\n" | passwd $USER 
chsh -s /bin/bash service

### --  SSH KEY GENERATION -- ###

USER_HOME=/home/$USER
SCRIPTS=$USER_HOME/scripts
IDEA=$USER_HOME/IdeaProjects
TMP=$USER_HOME/tmp

mkdir -p $USER_HOME/.ssh

echo echo "Generate SSH Keys"

ssh-keygen -t rsa -f $USER_HOME/.ssh/id_rsa -C "$USER@estangi.com" -N ""

cat /root/.ssh/authorized_keys  > $USER_HOME/.ssh/authorized_keys

echo
echo "Public key :"
echo
cat $USER_HOME/.ssh/id_rsa.pub
echo

### -- DOCKER -- ###

groupadd docker
gpasswd -a $USER docker

mkdir $IDEA
mkdir $SCRIPTS
cd $SCRIPTS
git archive --remote=ssh://git@gitlab.com/burninghead/environment.git HEAD:linux/scripts | tar -x

mkdir $TMP
cd $TMP
git archive --remote=ssh://git@gitlab.com/burninghead/environment.git HEAD:linux/shell | tar -x

echo "" >> $USER_HOME/.bashrc
echo "" >> $USER_HOME/.bashrc
cat ./bashrc >> $USER_HOME/.bashrc

echo "" >> $USER_HOME/.bash_aliases
echo "" >> $USER_HOME/.bash_aliases
cat ./bash_aliases >> $USER_HOME/.bash_aliases

chown -R $USER $USER_HOME




### -- JAVA -- ###

groupadd java
gpasswd -a $USER java

